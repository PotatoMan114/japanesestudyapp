package com.example.japanesestudy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.japanesestudy.components.LabelledInput;
import com.example.japanesestudy.models.Group;
import com.example.japanesestudy.presenters.AddGroupPresenter;
import com.example.japanesestudy.presenters.EditGroupsPresenter;
import com.google.android.material.button.MaterialButton;

import java.util.List;

public class AddGroupActivity extends BaseActivity implements AddGroupPresenter.MVPView {
    AddGroupPresenter presenter;

    public static class Contract extends ActivityResultContract<String, Boolean> {

        @NonNull
        @Override
        public Intent createIntent(@NonNull Context context, String input) {
            Intent intent = new Intent(context, AddGroupActivity.class);
            return intent;
        }

        @Override
        public Boolean parseResult(int resultCode, @Nullable Intent intent) {
            return true;

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);

        presenter = new AddGroupPresenter(this);

        LabelledInput nameInput = (LabelledInput) findViewById(R.id.nameInput);

        MaterialButton addButton = (MaterialButton) findViewById(R.id.addButton);

        addButton.setOnClickListener(view -> {
            presenter.addGroup(nameInput.getUserInput().toString());
        });

    }

    @Override
    public void returnToEditGroupsPage() {
        runOnUiThread(() -> {
            finish();
        });

    }
}

