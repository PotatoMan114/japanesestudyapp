package com.example.japanesestudy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.example.japanesestudy.database.AppDatabase;
import com.example.japanesestudy.presenters.BaseMVPView;

public class BaseActivity extends AppCompatActivity  implements BaseMVPView {

    @Override
    public AppDatabase getContextDatabase() {
        return Room.databaseBuilder(
                getApplicationContext(),
                AppDatabase.class,
                "word-database").build();
    }

}