package com.example.japanesestudy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.japanesestudy.components.LabelledInput;
import com.example.japanesestudy.models.Group;
import com.example.japanesestudy.presenters.CreateWordPresenter;
import com.example.japanesestudy.presenters.EditSearchPresenter;
import com.example.japanesestudy.presenters.MenuPresenter;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

public class CreateWordActivity extends BaseActivity implements CreateWordPresenter.MVPView {

    CreateWordPresenter presenter;

    String groupName;

    public static class Contract extends ActivityResultContract<String, Boolean> {

        @NonNull
        @Override
        public Intent createIntent(@NonNull Context context, String input) {
            Intent intent = new Intent(context, CreateWordActivity.class);
            if (input != null) {
                intent.putExtra("groupName", input);
            }
            else {
                intent.putExtra("groupName", "null");
            }
            return intent;
        }

        @Override
        public Boolean parseResult(int resultCode, @Nullable Intent intent) {
            return true;

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_word);
        presenter = new CreateWordPresenter(this);


        Intent intent = getIntent();
        this.groupName = intent.getStringExtra("groupName");

        MaterialButton addButton = (MaterialButton) findViewById(R.id.addButton);


        Spinner groupInput = (Spinner) findViewById(R.id.groupInput);
//        //TODO: read in group possibilities from database
//
//        ArrayAdapter<CharSequence> groupAdapter = ArrayAdapter.createFromResource(this,
//                R.array.groups_array, android.R.layout.simple_spinner_item);
//        groupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        groupInput.setAdapter(groupAdapter);

        renderGroups();



        addButton.setOnClickListener(view -> {
            LabelledInput charactersInput = (LabelledInput) findViewById(R.id.charactersInput);
            String characters = charactersInput.getUserInput().toString();
            LabelledInput romanizationInput = (LabelledInput) findViewById(R.id.romanizationInput);
            String romanization = romanizationInput.getUserInput().toString();
            LabelledInput furiganaInput = (LabelledInput) findViewById(R.id.furiganaInput);
            String furigana = furiganaInput.getUserInput().toString();
            LabelledInput okuriganaInput= (LabelledInput) findViewById(R.id.okuriganaInput);
            String okurigana = okuriganaInput.getUserInput().toString();
            LabelledInput definitionInput = (LabelledInput) findViewById(R.id.definitionInput);
            String definition = definitionInput.getUserInput().toString();
            LabelledInput exampleSentenceInput = (LabelledInput) findViewById(R.id.exampleSentenceInput);
            String exampleSentence = exampleSentenceInput.getUserInput().toString();

            String group = groupInput.getSelectedItem().toString();
            CheckBox hasKanjiInput = (CheckBox) findViewById(R.id.hasKanjiInput);
            boolean hasKanji = hasKanjiInput.isChecked();

            presenter.addWord(
                    characters,
                    romanization,
                    furigana,
                    okurigana,
                    definition,
                    exampleSentence,
                    group,
                    hasKanji
            );
            Toast.makeText(this, "Added word " + characters, Toast.LENGTH_SHORT).show();
        });
    }


    @Override
    public void goBackToMenuPage() {
        finish();
    }

    public void renderGroups() {
        presenter.loadGroups();
    }

    @Override
    public void populateGroups(List<Group> groups) {
        runOnUiThread(() -> {
            Spinner groupInput = (Spinner) findViewById(R.id.groupInput);
            ArrayList<CharSequence> groupsString = new ArrayList<>();
            for (Group group : groups) {
                groupsString.add(group.name);
            }
            ArrayAdapter<CharSequence> groupAdapter = new ArrayAdapter<CharSequence>(this,
                    android.R.layout.simple_spinner_item,
                    groupsString);



            groupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            groupInput.setAdapter(groupAdapter);

            if (groupName != null && !groupName.equals("null")) {
                try {
                    groupInput.setSelection(groupAdapter.getPosition(groupName));
                }
                catch (Exception e) {
                    //default value is okay
                }
            }

        });
    }

}
