package com.example.japanesestudy;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.japanesestudy.components.Choice;
import com.example.japanesestudy.models.Word;
import com.example.japanesestudy.presenters.DefinitionMultipleChoicePresenter;
import com.google.android.material.button.MaterialButton;

import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class DefinitionMultipleChoiceActivity extends StudyActivity implements DefinitionMultipleChoicePresenter.MVPView {

    DefinitionMultipleChoicePresenter presenter;

    TextView wordDisplay;
    TextView readingDisplay;
    Choice optionOneView;
    Choice optionTwoView;
    Choice optionThreeView;
    Choice optionFourView;

    List<Word> words;
    ListIterator<Word> wordsIterator;
    String group;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiple_choice);

        presenter = new DefinitionMultipleChoicePresenter(this);

        Intent intent = getIntent();
        this.group = intent.getStringExtra("group");
        this.words = intent.getParcelableArrayListExtra("words");
        assert this.words != null; //TODO: learn what this is
        Collections.shuffle(this.words);

        wordsIterator = words.listIterator();

        wordDisplay = (TextView) findViewById(R.id.wordDisplay);
        readingDisplay = (TextView) findViewById(R.id.hintDisplay);
        optionOneView = (Choice) findViewById(R.id.firstOption);
        optionTwoView = (Choice) findViewById(R.id.secondOption);
        optionThreeView = (Choice) findViewById(R.id.thirdOption);
        optionFourView = (Choice) findViewById(R.id.fourthOption);

        presenter.renderItems();
        MaterialButton nextButton = findViewById(R.id.nextButton);
        nextButton.setOnClickListener(view -> {
            presenter.goToNextItem();
        });

        MaterialButton showResultButton = findViewById(R.id.showResultButton);
        showResultButton.setOnClickListener(view -> {
            presenter.revealAnswer();
        });

    }

    public void renderItems(){
        runOnUiThread(() -> {
            if (!wordsIterator.hasNext()) {
                Toast.makeText(this, "No words found in this group", Toast.LENGTH_SHORT).show();
            }
            else {
                presenter.renderItem(wordsIterator.next());
            }

        });
    }

    @Override
    public void goToNextItem() {

        if (wordsIterator.hasNext()) {
            presenter.renderItem(wordsIterator.next());
        }
        else {
            runOnUiThread(() -> {
                Toast.makeText(this, "No more words left in group.", Toast.LENGTH_SHORT).show();
            });

        }
    }

    @Override
    public void renderItem(Word word) {
        runOnUiThread(() -> {
            wordDisplay.setText(word.characters);
            readingDisplay.setText(word.furigana + word.okurigana);
            resetChoicesBackgrounds();

            presenter.loadChoices(this.words, word);
        });


    }

    //renders four options, one of which is correct
    public void renderChoices(List<Word> options, Word correctOption) {
        runOnUiThread(() -> {
            int max = 3;
            int min = 0;
            int correctOptionLocation = (int) Math.floor(Math.random()*(max-min+1)+min);
            options.add(correctOptionLocation, correctOption);

            Log.d("correctOptionLocation", ((Integer) correctOptionLocation).toString());
            Log.d("options", options.toString());

            switch (correctOptionLocation) {
                case 0:
                    optionOneView.setCorrect(true);
                    optionTwoView.setCorrect(false);
                    optionThreeView.setCorrect(false);
                    optionFourView.setCorrect(false);
                    break;
                case 1:
                    optionOneView.setCorrect(false);
                    optionTwoView.setCorrect(true);
                    optionThreeView.setCorrect(false);
                    optionFourView.setCorrect(false);
                    break;
                case 2:
                    optionOneView.setCorrect(false);
                    optionTwoView.setCorrect(false);
                    optionThreeView.setCorrect(true);
                    optionFourView.setCorrect(false);
                    break;
                case 3:
                    optionOneView.setCorrect(false);
                    optionTwoView.setCorrect(false);
                    optionThreeView.setCorrect(false);
                    optionFourView.setCorrect(true);
                    break;
            }

            optionOneView.setText(options.get(0).definition);
            optionTwoView.setText(options.get(1).definition);
            optionThreeView.setText(options.get(2).definition);
            optionFourView.setText(options.get(3).definition);
        });

    }

    public void choiceSelected(View view) {
        runOnUiThread(() -> {
//            Toast.makeText(this, ((Choice) view).getText() + " selected", Toast.LENGTH_SHORT).show();

            presenter.revealAnswer();
        });

    }

    public void revealAnswer() {
        runOnUiThread(() -> {
            if (optionOneView.isCorrect()) {
                optionOneView.setBackground(getDrawable(R.drawable.correct_option_background));
            }
            else {
                optionOneView.setBackground(getDrawable(R.drawable.incorrect_option_background));
            }
            if (optionTwoView.isCorrect()) {
                optionTwoView.setBackground(getDrawable(R.drawable.correct_option_background));
            }
            else {
                optionTwoView.setBackground(getDrawable(R.drawable.incorrect_option_background));
            }
            if (optionThreeView.isCorrect()) {
                optionThreeView.setBackground(getDrawable(R.drawable.correct_option_background));
            }
            else {
                optionThreeView.setBackground(getDrawable(R.drawable.incorrect_option_background));
            }
            if (optionFourView.isCorrect()) {
                optionFourView.setBackground(getDrawable(R.drawable.correct_option_background));
            }
            else {
                optionFourView.setBackground(getDrawable(R.drawable.incorrect_option_background));
            }
        });

    }

    public void resetChoicesBackgrounds() {
        optionOneView.setBackground(getDrawable(R.drawable.multiple_choice_option_background));
        optionTwoView.setBackground(getDrawable(R.drawable.multiple_choice_option_background));
        optionThreeView.setBackground(getDrawable(R.drawable.multiple_choice_option_background));
        optionFourView.setBackground(getDrawable(R.drawable.multiple_choice_option_background));
    }
}
