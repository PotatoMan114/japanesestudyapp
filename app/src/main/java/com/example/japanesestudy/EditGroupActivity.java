package com.example.japanesestudy;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.japanesestudy.components.ClickableWord;
import com.example.japanesestudy.components.GroupDisplay;
import com.example.japanesestudy.models.Group;
import com.example.japanesestudy.models.Word;
import com.example.japanesestudy.presenters.EditGroupPresenter;
import com.example.japanesestudy.presenters.EditGroupsPresenter;
import com.google.android.material.button.MaterialButton;

import java.util.List;

public class EditGroupActivity extends BaseActivity implements EditGroupPresenter.MVPView {
    EditGroupPresenter presenter;
    List<Word> words;
    Group group;

    private ActivityResultLauncher<Word> editWordCustomContract = registerForActivityResult(new EditWordActivity.Contract(), result -> {
        presenter.onEditWordCustomContractResult();
    });

    private ActivityResultLauncher<String> createWordCustomContract = registerForActivityResult(new CreateWordActivity.Contract(), result -> {
        presenter.onCreateWordCustomContractResult();
    });

    public static class Contract extends ActivityResultContract<Group, Boolean> {

        @NonNull
        @Override
        public Intent createIntent(@NonNull Context context, Group input) {
            Intent intent = new Intent(context, EditGroupActivity.class);
            intent.putExtra("group", input);
            return intent;
        }

        @Override
        public Boolean parseResult(int resultCode, @Nullable Intent intent) {
            return true;

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_group);

        presenter = new EditGroupPresenter(this);

        Intent intent = getIntent();
        this.group = (Group) intent.getParcelableExtra("group");

        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.mainLayout);

        renderWords();


        MaterialButton doneButton = (MaterialButton) findViewById(R.id.doneButton);
        doneButton.setOnClickListener(view -> {
            presenter.goBackToEditGroupsActivity();
        });

        ImageButton saveGroupNameButton = (ImageButton) findViewById(R.id.saveGroupNameButton);
        EditText groupNameInput = (EditText) findViewById(R.id.groupNameInput);
        groupNameInput.setText(group.name);
        saveGroupNameButton.setOnClickListener(view -> {
            saveGroupName();
        });

        MaterialButton addWordButton = (MaterialButton) findViewById(R.id.addWordButton);
        addWordButton.setOnClickListener(view -> {
            presenter.goToCreateWordActivity(group.name);
        });

    }

    public void saveGroupName() {
        EditText groupNameInput = (EditText) findViewById(R.id.groupNameInput);

        group.name = groupNameInput.getText().toString();
        presenter.saveGroupName(group);
        presenter.changeAllWordsGroups(words, groupNameInput.getText().toString());
    }

    public void renderWords() {
        presenter.loadWords(group.name);
    }

    public void populateWords(List<Word> words) {
        runOnUiThread(() -> {
            this.words = words;
            LinearLayout wordsLayout = (LinearLayout) findViewById(R.id.wordsLayout);
            wordsLayout.removeAllViews();
//            words.add(testWords);
            for (Word word : words) {
                ClickableWord clickableWord = new ClickableWord(this, word);
                clickableWord.setOnClickListener(view -> {
                    presenter.goToEditWordActivity(word);
                });

                clickableWord.setTextColor(getTextColorPrimary());
                clickableWord.setTextSize(20);
                clickableWord.setPadding(5, 15, 5, 15);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                clickableWord.setLayoutParams(params);

                clickableWord.setBackground(getDrawable(R.drawable.search_result_background_drawable));

                wordsLayout.addView(clickableWord);


            }

        });
    }

    @Override
    public void goBackToEditGroupsActivity() {
        saveGroupName();
        finish();
    }

    public int getTextColorPrimary() {
        //I have Stack Overflow to thank for this
        //https://stackoverflow.com/questions/33050999/programmatically-set-text-color-to-primary-android-textview
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getTheme();
        theme.resolveAttribute(android.R.attr.textColorPrimary, typedValue, true);
        TypedArray arr =
                obtainStyledAttributes(typedValue.data, new int[]{
                        android.R.attr.textColorPrimary});
        int color = arr.getColor(0, -1);
        arr.recycle();
        return color;
    }

    public void goToEditWordActivity(Word word) {
        editWordCustomContract.launch(word);
    }

    public void goToCreateWordActivity(String groupName) {
        createWordCustomContract.launch(groupName);
    }
}