package com.example.japanesestudy;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.japanesestudy.components.GroupDisplay;
import com.example.japanesestudy.models.Group;
import com.example.japanesestudy.models.Word;
import com.example.japanesestudy.presenters.EditGroupsPresenter;
import com.example.japanesestudy.presenters.MenuPresenter;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

public class EditGroupsActivity extends BaseActivity implements EditGroupsPresenter.MVPView{
    EditGroupsPresenter presenter;

    public static class Contract extends ActivityResultContract<String, Boolean> {

        @NonNull
        @Override
        public Intent createIntent(@NonNull Context context, String input) {
            Intent intent = new Intent(context, EditGroupsActivity.class);
            return intent;
        }

        @Override
        public Boolean parseResult(int resultCode, @Nullable Intent intent) {
            return true;

        }
    }

    private ActivityResultLauncher<String> addGroupCustomContract = registerForActivityResult(new AddGroupActivity.Contract(), result -> {
        presenter.onAddGroupActivityCustomContractResult();
    });

    private ActivityResultLauncher<Group> editGroupCustomContract = registerForActivityResult( new EditGroupActivity.Contract(), result -> {
        presenter.onEditGroupActivityCustomContractResult();
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_groups);

        presenter = new EditGroupsPresenter(this);

        renderGroups();

        MaterialButton addButton = (MaterialButton) findViewById(R.id.addButton);

        addButton.setOnClickListener(view -> {
            presenter.goToAddGroupActivity();
            presenter.loadGroups();
        });

    }

    public void renderGroups() {
        presenter.loadGroups();

    }

    @Override
    public void populateGroups(List<Group> groups) {
        runOnUiThread(() -> {
            LinearLayout groupsLayout = (LinearLayout) findViewById(R.id.groupsLayout);
            groupsLayout.removeAllViews();
            for (Group group : groups) {
                GroupDisplay groupDisplay = new GroupDisplay(this, group);
//                groupDisplay.setPadding(5, 5, 5, 5);
//                groupDisplay.setTextSize(20);
                groupsLayout.addView(groupDisplay);

                groupDisplay.editButton.setOnClickListener(view -> {
                    presenter.goToEditGroupActivity(group);
                });
                groupDisplay.deleteButton.setOnClickListener(view -> {
                    presenter.deleteGroup(group);
                });

            }

        });
    }

    @Override
    public void goToAddGroupActivity() {
//        Intent intent = new Intent(this, AddGroupActivity.class);
//        startActivity(intent);
        addGroupCustomContract.launch(null);
    }

    @Override
    public void goToEditGroupActivity(Group group) {
        editGroupCustomContract.launch(group);
    }
}

