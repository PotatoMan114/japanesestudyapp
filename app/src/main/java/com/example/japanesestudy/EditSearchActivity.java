package com.example.japanesestudy;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;

import com.example.japanesestudy.components.ClickableWord;
import com.example.japanesestudy.components.LabelledInput;
import com.example.japanesestudy.models.Word;
import com.example.japanesestudy.presenters.EditSearchPresenter;
import com.example.japanesestudy.presenters.MenuPresenter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

public class EditSearchActivity extends BaseActivity implements EditSearchPresenter.MVPView {
    EditSearchPresenter presenter;
    LinearLayout resultsLayout;

//    private ActivityResultLauncher<Intent> mStartForResult = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
//            new ActivityResultCallback<ActivityResult>) {
//
//    }

    private ActivityResultLauncher<Word> customContract = registerForActivityResult(new EditWordActivity.Contract(), result -> {
        presenter.onCustomContractResult(result);
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_search);

        presenter = new EditSearchPresenter(this);

        resultsLayout = new LinearLayout(this);
        resultsLayout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        resultsLayout.setLayoutParams(linearLayoutParams);

        LabelledInput searchInput = (LabelledInput) findViewById(R.id.editSearchInput);

        MaterialButton searchButton = (MaterialButton) findViewById(R.id.searchButton);
        searchButton.setOnClickListener(view -> {
            String search = searchInput.getUserInput().toString();
            presenter.searchDatabase(search);
        });

    }

    @Override
    public void fillResultsLayout(List<Word> results) {
        Toast.makeText(this, "This will make the results of the search", Toast.LENGTH_SHORT).show();

        for (Word word : results) {
            ClickableWord clickableWord = new ClickableWord(this, word);
            clickableWord.setOnClickListener(view -> {
                presenter.goToEditPage(clickableWord.getWord());
            });
            resultsLayout.addView(clickableWord);
        }
        Toast.makeText(this, "search and result display generation complete", Toast.LENGTH_SHORT).show();


    }


    @Override
    public void createClickableResult(Word result) {
        runOnUiThread(() -> {
            ClickableWord clickableWord = new ClickableWord(this, result);
            clickableWord.setOnClickListener(view -> {
                presenter.goToEditPage(clickableWord.getWord());
            });

            clickableWord.setTextColor(getTextColorPrimary());
            clickableWord.setTextSize(20);
            clickableWord.setPadding(5, 15, 5, 15);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            clickableWord.setLayoutParams(params);

            clickableWord.setBackground(getDrawable(R.drawable.search_result_background_drawable));

            resultsLayout.addView(clickableWord);

        });
    }

    @Override
    public void createNoResultMessage() {
        runOnUiThread(() -> {
            Toast.makeText(this, "No words were found that match.", Toast.LENGTH_LONG).show();
        });

    }

    @Override
    public void addResultLayout() {
        runOnUiThread(() -> {
            LinearLayout mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
//            resultsLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary, null));

            mainLayout.removeView(resultsLayout);
            mainLayout.addView(resultsLayout);
        });
    }

    public int getTextColorPrimary() {
        //I have Stack Overflow to thank for this
        //https://stackoverflow.com/questions/33050999/programmatically-set-text-color-to-primary-android-textview
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = getTheme();
        theme.resolveAttribute(android.R.attr.textColorPrimary, typedValue, true);
        TypedArray arr =
                obtainStyledAttributes(typedValue.data, new int[]{
                        android.R.attr.textColorPrimary});
        int color = arr.getColor(0, -1);
        arr.recycle();
        return color;
    }

    @Override
    public void goToEditPage(Word word) {
//        Intent intent = new Intent(this, EditWordActivity.class);
//        intent.putExtra("word", word);
//        //startActivityForResult(intent, 1);
//        //startActivityForResult has been deprecated.

        customContract.launch(word);
    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent words) {
//        super.onActivityResult(requestCode, resultCode, words);
//        if (resultCode == Activity.RESULT_OK) {
//            presenter.returnToMenuPage();
//        }
//    }

    public void returnToMenuPage() {
        finish();
    }
}
