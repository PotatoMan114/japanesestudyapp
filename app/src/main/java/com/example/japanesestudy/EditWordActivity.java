package com.example.japanesestudy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.japanesestudy.components.LabelledCheckBox;
import com.example.japanesestudy.components.LabelledInput;
import com.example.japanesestudy.models.Group;
import com.example.japanesestudy.models.Word;
import com.example.japanesestudy.presenters.EditWordPresenter;
import com.example.japanesestudy.presenters.MenuPresenter;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

public class EditWordActivity extends BaseActivity implements EditWordPresenter.MVPView {
    EditWordPresenter presenter;

    Word word;

    boolean hasKanji = true;

    public static class Contract extends ActivityResultContract<Word, Boolean> {

        @NonNull
        @Override
        public Intent createIntent(@NonNull Context context, Word input) {
            Intent intent = new Intent(context, EditWordActivity.class);
            intent.putExtra("word", input);
            return intent;
        }

        @Override
        public Boolean parseResult(int resultCode, @Nullable Intent intent) {
            if (resultCode == RESULT_OK) {
                //Go back to main menu
                return true;
            }
            else {
                //Just go back to previous activity
                return false;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_word);

        presenter = new EditWordPresenter(this);

        Intent intent = getIntent();
        this.word = (Word) intent.getParcelableExtra("word");

        TextView header = (TextView) findViewById(R.id.editHeader);
        header.setText("Edit the word \"" + word.characters + "\":");

        //prepopulate input fields
        LabelledInput charactersInput = (LabelledInput) findViewById(R.id.charactersInput);
        charactersInput.populateInput(word.characters);
        LabelledInput romanizationInput = (LabelledInput) findViewById(R.id.romanizationInput);
        romanizationInput.populateInput(word.romanization);
        LabelledInput furiganaInput = (LabelledInput) findViewById(R.id.furiganaInput);
        furiganaInput.populateInput(word.furigana);
        LabelledInput okuriganaInput = (LabelledInput) findViewById(R.id.okuriganaInput);
        okuriganaInput.populateInput(word.okurigana);
        LabelledInput definitionInput = (LabelledInput) findViewById(R.id.definitionInput);
        definitionInput.populateInput(word.definition);
        LabelledInput exampleSentenceInput = (LabelledInput) findViewById(R.id.exampleSentenceInput);
        exampleSentenceInput.populateInput(word.exampleSentence);


        Spinner groupInput = (Spinner) findViewById(R.id.groupInput);
//        ArrayAdapter<CharSequence> groupAdapter = ArrayAdapter.createFromResource(this,
//                R.array.groups_array, android.R.layout.simple_spinner_item);
//        groupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        groupInput.setAdapter(groupAdapter);
        renderGroups();



        CheckBox hasKanjiInput = (CheckBox) findViewById(R.id.hasKanjiInput);
        hasKanjiInput.setChecked(word.hasKanji);

        //give buttons OnClickListeners
        MaterialButton deleteButton = (MaterialButton) findViewById(R.id.deleteButton);
        deleteButton.setOnClickListener(view -> {
            presenter.deleteWord(word);
        });

        MaterialButton editButton = (MaterialButton) findViewById(R.id.editButton);
        editButton.setOnClickListener(view -> {
            Word newWord = new Word();

            newWord.id = word.id;

            newWord.characters = charactersInput.getUserInput().toString();
            newWord.romanization = romanizationInput.getUserInput().toString();
            newWord.furigana = furiganaInput.getUserInput().toString();
            newWord.okurigana = okuriganaInput.getUserInput().toString();
            newWord.definition = definitionInput.getUserInput().toString();
            newWord.exampleSentence = exampleSentenceInput.getUserInput().toString();
            newWord.group = groupInput.getSelectedItem().toString();
            newWord.hasKanji = hasKanjiInput.isChecked();

            presenter.editWord(newWord);
        });

    }



    @Override
    public void returnToMenuPage() {
//        runOnUiThread(() -> {
//            Toast.makeText(this,
//                    "You should now go back to the main menu",
//                    Toast.LENGTH_SHORT).show();
//        });
        Intent resultIntent = new Intent();
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    public void renderGroups() {
        presenter.loadGroups();
    }

    @Override
    public void populateGroups(List<Group> groups) {
        runOnUiThread(() -> {
            Spinner groupInput = (Spinner) findViewById(R.id.groupInput);
            ArrayList<CharSequence> groupsString = new ArrayList<>();
            for (Group group : groups) {
                groupsString.add(group.name);
            }
            ArrayAdapter<CharSequence> groupAdapter = new ArrayAdapter<CharSequence>(this,
                    android.R.layout.simple_spinner_item,
                    groupsString);

            groupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            groupInput.setAdapter(groupAdapter);

            try {
                int prePopulationPosition = groupAdapter.getPosition(word.group);
                groupInput.setSelection(prePopulationPosition);

            }
            catch (Exception e) {
                Toast.makeText(this, "The group this word belongs to is no longer available", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public void goToPreviousPage() {
        finish();
    }

    @Override
    public void done() {
        goToPreviousPage();
    }


}