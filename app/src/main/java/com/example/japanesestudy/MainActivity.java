package com.example.japanesestudy;

import com.example.japanesestudy.models.Group;
import com.example.japanesestudy.models.Word;
import com.example.japanesestudy.presenters.MenuPresenter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

//Main Menu

public class MainActivity extends BaseActivity implements MenuPresenter.MVPView, AdapterView.OnItemSelectedListener {
    final String GROUPS_FILE_NAME = "groups_app_save.txt";
    final String WORDS_FILE_NAME = "words_app_save.txt";
    MenuPresenter presenter;

    private ActivityResultLauncher<String> editGroupsCustomContract = registerForActivityResult(new EditGroupsActivity.Contract(), result -> {
        presenter.onEditGroupsCustomContractResult();
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MenuPresenter(this);




        Spinner groupPicker = (Spinner) findViewById(R.id.groupPicker);

        renderGroups();



        Spinner methodPicker = (Spinner) findViewById(R.id.methodPicker);

        ArrayAdapter<CharSequence> methodAdapter = ArrayAdapter.createFromResource(this,
                R.array.methods_array, android.R.layout.simple_spinner_item);
        methodAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        methodPicker.setAdapter(methodAdapter);
        methodPicker.setOnItemSelectedListener(this);

        MaterialButton studyButton = (MaterialButton) findViewById(R.id.studyButton);

        studyButton.setOnClickListener(view -> {
            presenter.goToStudyPage(groupPicker.getSelectedItem().toString(),
                    methodPicker.getSelectedItem().toString());

        });

        FloatingActionButton createWordButton = (FloatingActionButton) findViewById(R.id.createWordButton);

        createWordButton.setImageResource(R.drawable.ic_baseline_edit_24);
        createWordButton.setOnClickListener(view -> {
            new MaterialAlertDialogBuilder(this)
                    .setTitle("Choose")
                    .setItems(new CharSequence[]{"Insert Word",
                            "Edit Word",
                            "Edit Groups",
                            "Load Default Data",
                            "DEV: External Save"},
                            (menuItem, i) -> {
                        if (i == 0) {
                            //insert word
                            presenter.goToCreateWordPage();
                        }
                        else if (i == 1) {
                            //edit word
                            presenter.goToEditWordPage();
                        }
                        else if (i == 2){
                            presenter.goToEditGroupsPage();
                        }
                        else if (i ==3) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setMessage(R.string.load_data_dialog_message);
                            builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            loadDataFromFile();
                                            dialogInterface.dismiss();
                                        }
                                    });
                            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });

                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                        else {
                            //Access this data through Android Studio
                            //View -> Tool Windows -> Device File Explorer
                            //data/data/com.example.japanesestudy/files
                            presenter.externalSave();
                        }
                    }).show();
        });


    }

    private void loadDataFromFile() {
        presenter.loadWordsFromFile(this, R.raw.words);
        presenter.loadGroupsFromFile(this, R.raw.groups);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        //Toast.makeText(parent.getContext(), text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void goToReadingMultipleChoicePage(String group, List<Word> words) {
        runOnUiThread(() -> {
            Toast.makeText(this, "Going to Reading Multiple Choice with group " + group, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, ReadingMultipleChoiceActivity.class);
            intent.putExtra("group", group);
            intent.putParcelableArrayListExtra("words", (ArrayList<Word>) words);
            startActivity(intent);
        });
    }

    @Override
    public void goToDefinitionMultipleChoicePage(String group, List<Word> words) {
        runOnUiThread(() -> {
            Toast.makeText(this, "Going to Definition Multiple Choice with group " + group, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, DefinitionMultipleChoiceActivity.class);
            intent.putExtra("group", group);
            intent.putParcelableArrayListExtra("words", (ArrayList<Word>) words);
            startActivity(intent);
        });
    }

    @Override
    public void goToReadingRecognitionPage(String group) {
        Toast.makeText(this, "Going to Reading Recognition with group " + group, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void goToDefinitionRecognitionPage(String group) {
        Toast.makeText(this, "Going to Definition Recognition with group " + group, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void goToFuriganaToKanjiPage(String group) {
        Toast.makeText(this, "Going to Furigana to Kanji with group " + group, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void goToENtoJPMultipleChoicePage(String group) {
        Toast.makeText(this, "Going to EN to JP Multiple Choice with group " + group, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void goToCreateWordPage() {
        Intent intent = new Intent(this, CreateWordActivity.class);
        startActivity(intent);
    }

    @Override
    public void goToEditWordPage() {
        Intent intent = new Intent(this, EditSearchActivity.class);
        startActivity(intent);
    }

    @Override
    public void goToEditGroupsPage() {
//        Intent intent = new Intent(this, EditGroupsActivity.class);
        editGroupsCustomContract.launch(null);
    }


    public void renderGroups() {
        presenter.loadGroups();

    }

    @Override
    public void populateGroups(List<Group> groups) {
        runOnUiThread(() -> {
            Spinner groupPicker = (Spinner) findViewById(R.id.groupPicker);
//
//            ArrayAdapter<CharSequence> groupAdapter = ArrayAdapter.createFromResource(this,
//                    R.array.groups_array, android.R.layout.simple_spinner_item);
//            groupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//            groupPicker.setAdapter(groupAdapter);
//            groupPicker.setOnItemSelectedListener(this);

            ArrayList<CharSequence> groupsString = new ArrayList<>();
            for (Group group : groups) {
                groupsString.add(group.name);
            }
            groupsString.add("All");
            ArrayAdapter<CharSequence> groupAdapter = new ArrayAdapter<CharSequence>(this,
                    android.R.layout.simple_spinner_item,
                    groupsString);
            groupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            groupPicker.setAdapter(groupAdapter);
        });
    }

    public void externalSave(String groups, String words) {
        runOnUiThread(() -> {
        FileOutputStream fos = null;



        //groups
        try {
            fos = openFileOutput(GROUPS_FILE_NAME, MODE_PRIVATE);
            fos.write(groups.getBytes(StandardCharsets.UTF_8));
            Log.d("groups", groups.getBytes().toString());
            Toast.makeText(this, "Wrote groups to file: " + getFilesDir() + "/" + GROUPS_FILE_NAME,
                    Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        //words
        try {
            fos = openFileOutput(WORDS_FILE_NAME, MODE_PRIVATE);
            fos.write(words.getBytes());
            Log.d("words", words);

            Toast.makeText(this, "Wrote words to file: " + getFilesDir() + "/" + WORDS_FILE_NAME,
                    Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        });

        //TEST
        FileInputStream fis = null;

        try {
            fis = openFileInput(WORDS_FILE_NAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String text;

            while ((text = br.readLine()) != null) {
                sb.append(text).append("\n");
            }

            Log.d("words from file", sb.toString());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}