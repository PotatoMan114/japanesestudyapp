package com.example.japanesestudy.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class Choice extends androidx.appcompat.widget.AppCompatTextView {

    boolean correctOption = false;
    public Choice(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setCorrect(boolean correct) {
        this.correctOption = correct;
    }

    public boolean isCorrect() {
        return correctOption;
    }
}
