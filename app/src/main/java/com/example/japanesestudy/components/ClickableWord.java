package com.example.japanesestudy.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.example.japanesestudy.R;
import com.example.japanesestudy.models.Word;

public class ClickableWord extends LinearLayout {

    private Word word;
    private TextView textView;

    public ClickableWord(Context context, Word word) {
        super(context);

        this.word = word;

        textView = new TextView(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        setLayoutParams(params);
        textView.setText(word.characters);
//        textView.setOnClickListener(view -> {
//            Toast.makeText(context, "You clicked on " + this.word.characters, Toast.LENGTH_SHORT).show();
//        };

        setClickable(true);

        addView(textView);
    }

    public Word getWord() {
        return this.word;
    }

    public void setTextSize(float size) {
        textView.setTextSize(size);
    }

    public void setTextColor(int color) {
        textView.setTextColor(color);
    }
}