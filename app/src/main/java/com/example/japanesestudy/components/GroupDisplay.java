package com.example.japanesestudy.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;

import com.example.japanesestudy.R;
import com.example.japanesestudy.models.Group;
import com.google.android.material.button.MaterialButton;

public class GroupDisplay extends LinearLayout {
    TextView groupNameView;
    public ImageButton deleteButton;
    public ImageButton editButton;
    Group group;

    public GroupDisplay(Context context, Group group) {
        super(context);

        this.group = group;
        setOrientation(HORIZONTAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(5, 5, 5, 5);
        setLayoutParams(params);

        groupNameView = new TextView(context);
        groupNameView.setText(group.name);
        params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.weight = 1;
        groupNameView.setLayoutParams(params);

        deleteButton = new ImageButton(context);
        deleteButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_delete_24, null));
        deleteButton.setBackground(getResources().getDrawable(R.drawable.button_icon_background, null));
        editButton = new ImageButton(context);
        editButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_edit_24, null));
        editButton.setBackground(getResources().getDrawable(R.drawable.button_icon_background, null));

        LayoutParams buttonParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        buttonParams.gravity = Gravity.RIGHT;
        buttonParams.setMargins(5, 5, 5,5);
        editButton.setLayoutParams(buttonParams);
        deleteButton.setLayoutParams(buttonParams);
        editButton.setPadding(15, 15, 15, 15);
        deleteButton.setPadding(15, 15, 15, 15);

        addView(groupNameView);
        addView(deleteButton);
        addView(editButton);
    }

//    public GroupDisplay(Context context, Group group) {
//        this(context);
//
//        setGroup(group);
//
//    }



    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
        groupNameView.setText(group.name);
    }
    public void setTextSize(float size) {
        groupNameView.setTextSize(size);
    }

    public void setTextColor(int color) {
        groupNameView.setTextColor(color);
    }

//    public void setDeleteButtonOnClickListener()
    //TODO: figure this out, need to accept a lambda
}

