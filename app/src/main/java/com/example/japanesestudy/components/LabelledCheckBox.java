package com.example.japanesestudy.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.example.japanesestudy.R;

public class LabelledCheckBox extends LinearLayout {
    AppCompatTextView labelView;
    CheckBox checkBox;
    String labelText;
    Integer labelPos;

    public LabelledCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);


        TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(attrs, R.styleable.LabelledCheckBox, 0, 0);
        this.setOrientation(LinearLayout.HORIZONTAL);

        try {
            labelText = styledAttributes.getString(R.styleable.LabelledCheckBox_checkBoxLabelText);
            labelPos = styledAttributes.getInteger(R.styleable.LabelledCheckBox_checkBoxPosition, 0);
        }
        finally {
            styledAttributes.recycle();
        }
        labelView = new AppCompatTextView(context);
        checkBox = new CheckBox(context);

        labelView.setText(labelText);

        if (labelPos == 0) {
            addView(labelView);
            addView(checkBox);
        }
        else {
            addView(checkBox);
            addView(labelView);
        }

    }

    public boolean isChecked() {
        return checkBox.isChecked();
    }

    public void setChecked(boolean selected) {
        checkBox.setChecked(selected);
    }

    public String getLabel() {
        return (String)labelView.getText();
    }
}