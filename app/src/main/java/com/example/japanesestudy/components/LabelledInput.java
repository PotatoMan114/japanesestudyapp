package com.example.japanesestudy.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.example.japanesestudy.R;

public class LabelledInput extends LinearLayout {
    enum labelPosition {
        top,
        left,
    }

    AppCompatTextView labelView;
    AppCompatEditText inputView;
    String labelText;
    Integer labelPos;

    public LabelledInput(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(attrs, R.styleable.LabelledInput, 0, 0);

        try {
            labelText = styledAttributes.getString(R.styleable.LabelledInput_labelText);
            labelPos = styledAttributes.getInteger(R.styleable.LabelledInput_labelPosition, 0);
        }
        finally {
            styledAttributes.recycle();
        }

        if (labelPos == 0) {
            this.setOrientation(LinearLayout.VERTICAL);
        }
        else {
            this.setOrientation(LinearLayout.HORIZONTAL);
        }


        labelView = new AppCompatTextView(context);
        inputView = new AppCompatEditText(context);

        labelView.setText(labelText);

        addView(labelView);
        addView(inputView);
    }

    public Editable getUserInput() {
        return inputView.getText();
    }

    public String getLabel() {
        return (String)labelView.getText();
    }

    public void populateInput(String text) {
        inputView.setText(text);
    }

}