package com.example.japanesestudy.database;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.japanesestudy.models.Group;
import com.example.japanesestudy.models.Word;

import java.util.concurrent.Executors;

@Database(entities = {Word.class, Group.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

//    private static AppDatabase INSTANCE;

    public abstract WordDao getWordDao();

    public abstract GroupDao getGroupDao();
//
//    public synchronized static AppDatabase getInstance(Context context) {
//        if (INSTANCE == null) {
//            INSTANCE = buildDatabase(context);
//        }
//        return INSTANCE;
//    }
//
//    private static AppDatabase buildDatabase(final Context context) {
//        return Room.databaseBuilder(context,
//                AppDatabase.class,
//                "my-database")
//                .addCallback(new Callback() {
//                    @Override
//                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
//                        super.onCreate(db);
//
//                        Executors.newSingleThreadExecutor().execute(new Runnable() {
//                            @Override
//                            public void run() {
//                                getInstance(context).getWordDao().insertAll(Word.populateData());
//                                getInstance(context).getGroupDao().insertAll(Group.populateData());
//                            }
//                        });
//                    }
//                }).build();
//    }

}