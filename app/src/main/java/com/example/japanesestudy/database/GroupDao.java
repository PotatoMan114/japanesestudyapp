package com.example.japanesestudy.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.TypeConverter;
import androidx.room.Update;

import com.example.japanesestudy.models.Group;
import com.example.japanesestudy.models.Word;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Dao
public interface GroupDao {

    @Query("SELECT * FROM `group`")
    List<Group> getAllGroups();

    @Insert
    void insert(Group group);

    @Update
    void update(Group group);

    @Delete
    void delete(Group group);

    @Insert
    void insertAll(Group... groups);

    @Query("DELETE FROM `group`")
    void nukeData();
}