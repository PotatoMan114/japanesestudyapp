package com.example.japanesestudy.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.TypeConverter;
import androidx.room.Update;

import com.example.japanesestudy.models.Word;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Dao
public interface WordDao {
    @Query("SELECT * FROM word")
    List<Word> getAllWords();

    @Query("SELECT * FROM word WHERE `group` LIKE :search") //works if group is a string, so a word can only fit in one group
    List<Word> getAllWordsInGroup(String search);

    @Query("SELECT * FROM word WHERE `group` LIKE :search AND `hasKanji` = 1")
    List<Word> getAllWordsInGroupWithKanji(String search);

    @Query("SELECT * FROM word WHERE characters LIKE :search")
    List<Word> searchForWord(String search);

    //make a query to get a word based on a group for if a word can fit in multiple groups

    @Query("SELECT * FROM word WHERE id = :search")
    Word getWordByID(String search);

    @Query("SELECT * FROM word WHERE `hasKanji` = 1")
    List<Word> getAllWordsWithKanji();


    @Insert
    void insert(Word word);

    @Insert
    void insertAll(Word... words);

    @Update
    void update(Word word);

    @Delete
    void delete(Word word);

    @Query("DELETE FROM `word`")
    void nukeData();


}