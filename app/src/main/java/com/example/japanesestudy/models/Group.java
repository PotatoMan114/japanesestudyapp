package com.example.japanesestudy.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;


@Entity
public class Group implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "name")
    public String name;

    @Override
    public String toString() {
        return name;
    }

    public Group() {

    }

    public Group(String name) {
        this.name = name;
    }

//    //I would like for this to come from a file, like a csv
//    public static Group[] populateData() {
//        return new Group[] {
//                new Group("Numbers"),
//                new Group("Weather"),
//                new Group("Calendar"),
//                new Group("Around Town"),
//                new Group("Prepopulated Group"),
//        };
//    }

    @Ignore
    @Override
    public int describeContents() {
        return 0;
    }

    @Ignore
    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeInt(id);
        out.writeString(name);

    }

    @Ignore
    public Group(Parcel source) {
        id = source.readInt();
        name = source.readString();
    }

    @Ignore
    public static final Parcelable.Creator<Group> CREATOR  = new Parcelable.Creator<Group>() {
        public Group createFromParcel(Parcel in) {
            return new Group(in);
        }
        public Group[] newArray(int size) {
            return new Group[size];
        }
    };
}