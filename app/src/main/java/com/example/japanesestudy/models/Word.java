package com.example.japanesestudy.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Word implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "characters")
    public String characters;

    @ColumnInfo(name = "romanization")
    public String romanization;

    @ColumnInfo(name = "furiagana")
    public String furigana;

    @ColumnInfo(name = "okurigana")
    public String okurigana;

    @ColumnInfo(name = "definition")
    public String definition;

    @ColumnInfo(name = "exampleSentence")
    public String exampleSentence;

    @ColumnInfo(name = "group")
    //want this to be an arraylist so that one word can fit into multiple groups
    public String group;

    @ColumnInfo(name = "hasKanji")
    public Boolean hasKanji;

    @Override
    public String toString() {
        return characters;
    }

    public Word() {

    }

    public Word(String characters,
                String romanization,
                String furigana,
                String okurigana,
                String definition,
                String exampleSentence,
                String group,
                Boolean hasKanji) {
        this.characters = characters;
        this.romanization = romanization;
        this. furigana = furigana;
        this.okurigana = okurigana;
        this.definition = definition;
        this.exampleSentence = exampleSentence;
        this.group = group;
        this.hasKanji = hasKanji;

    }

    //I would like for this to come from a file, like a csv

//    public static Word[] populateData() {
//        return new Word[] {
//                new Word("一", "ichi", "いち", "", "One", "", "Numbers", true),
//        };
//    }



    @Ignore
    @Override
    public int describeContents() {
        return 0;
    }

    @Ignore
    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeInt(id);
        String[] stringData = {
                characters,
                romanization,
                furigana,
                okurigana,
                definition,
                exampleSentence,
                group
        };
        out.writeStringArray(stringData);

        //Thank you stackOverflow
        //https://stackoverflow.com/questions/6201311/how-to-read-write-a-boolean-when-implementing-the-parcelable-interface
        out.writeByte((byte) (hasKanji ? 1 : 0));     //if myBoolean == true, byte == 1
    }

    @Ignore
    public Word(Parcel source) {
        id = source.readInt();
        String[] stringData = new String[7];
        source.readStringArray(stringData);
        characters = stringData[0];
        romanization = stringData[1];
        furigana = stringData[2];
        okurigana = stringData[3];
        definition = stringData[4];
        exampleSentence = stringData[5];
        group = stringData[6];

        //Thank you stackOverflow
        //https://stackoverflow.com/questions/6201311/how-to-read-write-a-boolean-when-implementing-the-parcelable-interface
        hasKanji = source.readByte() != 0;     //myBoolean == true if byte != 0

    }

    @Ignore
    public static final Parcelable.Creator<Word> CREATOR  = new Parcelable.Creator<Word>() {
        public Word createFromParcel(Parcel in) {
            return new Word(in);
        }
        public Word[] newArray(int size) {
            return new Word[size];
        }
    };

}
