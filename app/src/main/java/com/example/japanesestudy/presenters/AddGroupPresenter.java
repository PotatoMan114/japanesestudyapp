package com.example.japanesestudy.presenters;

import com.example.japanesestudy.database.AppDatabase;
import com.example.japanesestudy.models.Group;

import java.util.List;

public class AddGroupPresenter {
    private MVPView view;
    private AppDatabase database;

    public interface MVPView extends BaseMVPView {
        void returnToEditGroupsPage();
    }

    public AddGroupPresenter(AddGroupPresenter.MVPView view) {
        this.view = view;
        database = view.getContextDatabase();
    }

    public void addGroup(String name) {
        new Thread(() -> {
            Group group = new Group();
            group.name = name;
            database.getGroupDao().insert(group);
            view.returnToEditGroupsPage();
        }).start();

    }
}