package com.example.japanesestudy.presenters;

import com.example.japanesestudy.models.Word;

import java.util.List;

public interface BaseMCStudyPresenter extends BaseStudyPresenter {

    void loadChoices(List<Word> words, Word correctOption);

}
