package com.example.japanesestudy.presenters;

import com.example.japanesestudy.database.AppDatabase;

public interface BaseMVPView {
    public AppDatabase getContextDatabase();
}