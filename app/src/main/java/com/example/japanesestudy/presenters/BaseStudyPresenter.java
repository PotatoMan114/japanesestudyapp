package com.example.japanesestudy.presenters;

import com.example.japanesestudy.models.Word;

import java.util.List;

public interface BaseStudyPresenter {

    void goToNextItem();

    void renderItems();

    void returnToMainMenu();

    void revealAnswer();
}
