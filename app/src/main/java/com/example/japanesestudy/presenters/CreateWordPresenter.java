package com.example.japanesestudy.presenters;

import com.example.japanesestudy.database.AppDatabase;
import com.example.japanesestudy.models.Group;
import com.example.japanesestudy.models.Word;

import java.util.ArrayList;
import java.util.List;

public class CreateWordPresenter {
    private CreateWordPresenter.MVPView view;
    private AppDatabase database;

    public interface MVPView extends BaseMVPView {
        public void goBackToMenuPage();
        public void populateGroups(List<Group> groups);

    }

    public CreateWordPresenter(CreateWordPresenter.MVPView view) {
        this.view = view;

        database = view.getContextDatabase();


    }

    public void goBackToMenuPage() {
        view.goBackToMenuPage();
    }

    public void addWord(
            String characters,
            String romanization,
            String furiagna,
            String okurigana,
            String definition,
            String exampleSentence,
            String group,
            Boolean hasKanji) {

        Word word = new Word();
        word.characters = characters;
        word.romanization = romanization;
        word.furigana = furiagna;
        word.okurigana = okurigana;
        word.definition = definition;
        word.exampleSentence = exampleSentence;
        word.group = group;
        word.hasKanji = hasKanji;

        new Thread(() -> {
            database.getWordDao().insert(word);
            goBackToMenuPage();
        }).start();


    }

    public void loadGroups() {
        new Thread(() -> {
            List<Group> groups = database.getGroupDao().getAllGroups();
            view.populateGroups(groups);
        }).start();
    }
}
