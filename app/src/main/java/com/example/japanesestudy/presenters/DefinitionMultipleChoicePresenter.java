package com.example.japanesestudy.presenters;

import android.content.Intent;

import com.example.japanesestudy.database.AppDatabase;
import com.example.japanesestudy.database.WordDao;
import com.example.japanesestudy.models.Word;

import java.util.ArrayList;
import java.util.List;

public class DefinitionMultipleChoicePresenter implements BaseMCStudyPresenter {

    private MVPView view;
    private AppDatabase database;

    public interface MVPView extends BaseMVPView {
        void renderItems();
        void renderItem(Word word);
        void goToNextItem();
        void revealAnswer();
        void renderChoices(List<Word> choices, Word correctOption);
    }

    public DefinitionMultipleChoicePresenter(DefinitionMultipleChoicePresenter.MVPView view) {
        this.view = view;

        database = view.getContextDatabase();

    }

    @Override
    public void loadChoices(List<Word> words, Word correctOption) {
        getRandomWordsFromList(words, correctOption);
    }

    public void getRandomWordsFromList(List<Word> words, Word correctOption) {
        new Thread(() -> {
            ArrayList<Word> choices = new ArrayList<>();
            int max = words.size() - 1;
            int min = 0;
            for (int i = 0; i < 3; i++) { //make three incorrect options
                int optionIndex = (int) Math.floor(Math.random()*(max-min+1)+min);
                if (!words.get(optionIndex).characters.equals(correctOption.characters) &&
                        !choices.contains(words.get(optionIndex))) {
                    choices.add(words.get(optionIndex));
                }
                else {
                    i--;
                    continue;
                }
            }
            view.renderChoices(choices, correctOption);
        }).start();
    }

    @Override
    public void goToNextItem() {
        view.goToNextItem();
    }


    @Override
    public void renderItems() {
        view.renderItems();
    }

    @Override
    public void returnToMainMenu() {

    }

    @Override
    public void revealAnswer() {
        view.revealAnswer();
    }

    public void renderItem(Word word) {
        view.renderItem(word);
    }
}
