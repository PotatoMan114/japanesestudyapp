package com.example.japanesestudy.presenters;

import com.example.japanesestudy.database.AppDatabase;
import com.example.japanesestudy.models.Group;
import com.example.japanesestudy.models.Word;

import java.util.List;

public class EditGroupPresenter {
    private MVPView view;
    private AppDatabase database;

    public void onCreateWordCustomContractResult() {
        view.renderWords();
    }

    public interface MVPView extends BaseMVPView {
        void goBackToEditGroupsActivity();
        void populateWords(List<Word> words);
        void goToEditWordActivity(Word word);
        void renderWords();
        void goToCreateWordActivity(String groupName);
    }

    public EditGroupPresenter(EditGroupPresenter.MVPView view) {
        this.view = view;
        database = view.getContextDatabase();
    }

    public void goBackToEditGroupsActivity() {
        view.goBackToEditGroupsActivity();
    }

    public void loadWords(String group) {
        new Thread(() -> {
            List<Word> words = database.getWordDao().getAllWordsInGroup(group);
            view.populateWords(words);
        }).start();
    }

    public void goToEditWordActivity(Word word) {
        view.goToEditWordActivity(word);
    }

    public void onEditWordCustomContractResult() {
        view.renderWords();
    }

    public void saveGroupName(Group group) {
        new Thread(() -> {
            database.getGroupDao().update(group);
        }).start();
    }

    public void changeAllWordsGroups(List<Word> words, String newGroupName) {
        new Thread(() -> {
            for (Word word : words) {
                word.group = newGroupName;
                database.getWordDao().update(word);
            }
        }).start();
    }

    public void goToCreateWordActivity(String groupName) {
        view.goToCreateWordActivity(groupName);
    }
}