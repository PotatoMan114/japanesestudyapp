package com.example.japanesestudy.presenters;

import com.example.japanesestudy.database.AppDatabase;
import com.example.japanesestudy.models.Group;
import com.example.japanesestudy.models.Word;

import java.util.ArrayList;
import java.util.List;

public class EditGroupsPresenter {
    private MVPView view;
    private AppDatabase database;

    public interface MVPView extends BaseMVPView {
        void populateGroups(List<Group> groups);
        void goToAddGroupActivity();
        void goToEditGroupActivity(Group group);
    }

    public EditGroupsPresenter(EditGroupsPresenter.MVPView view) {
        this.view = view;
        database = view.getContextDatabase();
    }

    public void loadGroups() {
        new Thread(() -> {
            List<Group> groups = database.getGroupDao().getAllGroups();
            view.populateGroups(groups);
        }).start();
    }

    public void goToAddGroupActivity() {
        view.goToAddGroupActivity();
    }

    public void goToEditGroupActivity(Group group) {
        view.goToEditGroupActivity(group);
    }

    public void deleteGroup(Group group) {
        new Thread(() -> {
            database.getGroupDao().delete(group);
            loadGroups();
        }).start();
    }

    public void onAddGroupActivityCustomContractResult() {
        loadGroups();
    }

    public void onEditGroupActivityCustomContractResult() {
        loadGroups();
    }



}