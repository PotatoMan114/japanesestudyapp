package com.example.japanesestudy.presenters;

import com.example.japanesestudy.database.AppDatabase;
import com.example.japanesestudy.models.Word;

import java.util.ArrayList;
import java.util.List;

public class EditSearchPresenter {

    private EditSearchPresenter.MVPView view;
    private AppDatabase database;

    public interface MVPView extends BaseMVPView {
        public void fillResultsLayout(List<Word> results);
        public void goToEditPage(Word word);
        public void createClickableResult(Word result);
        public void addResultLayout();
        public void createNoResultMessage();
        public void returnToMenuPage();
    }

    public EditSearchPresenter(EditSearchPresenter.MVPView view) {
        this.view = view;

        database = view.getContextDatabase();

    }

    public void fillResultsLayout(List<Word> results) {
        view.fillResultsLayout(results);
    }

//    public List<Word> searchDatabase(String searchKey) {
//        return database.getWordDao().searchForWord(searchKey);
//    }

    public void searchDatabase (String searchKey) {
        new Thread(() -> {
            List<Word> results = database.getWordDao().searchForWord(searchKey);
//            List<Word> results = database.getWordDao().getAllWords();
            if (results.toArray().length == 0) {
                createNoResultMessage();
            }
            else {
                for (Word result : results) {
                    view.createClickableResult(result);
                }
                view.addResultLayout();
            }
        }).start();
    }

    public void createNoResultMessage() {
        view.createNoResultMessage();
    }

    public void goToEditPage(Word word) {
        view.goToEditPage(word);
    }

    public void returnToMenuPage() {
        view.returnToMenuPage();
    }

    public void onCustomContractResult(Boolean result) {
        if (result) {
            returnToMenuPage();
        }
        else {
            //do nothing really
        }
    }

}
