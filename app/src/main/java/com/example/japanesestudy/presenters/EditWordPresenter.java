package com.example.japanesestudy.presenters;

import com.example.japanesestudy.database.AppDatabase;
import com.example.japanesestudy.models.Group;
import com.example.japanesestudy.models.Word;

import java.util.List;

public class EditWordPresenter {

    private EditWordPresenter.MVPView view;
    private AppDatabase database;

    public interface MVPView extends BaseMVPView {
        public void returnToMenuPage();
        public void populateGroups(List<Group> groups);
        void goToPreviousPage();
        void done();
    }

    public EditWordPresenter(EditWordPresenter.MVPView view) {
        this.view = view;

        this.database = view.getContextDatabase();
    }

    public void deleteWord(Word word) {
        new Thread(() -> {
            database.getWordDao().delete(word);
//            view.returnToMenuPage();
            view.done();
        }).start();
    }

    public void editWord(Word word) {
        new Thread(() -> {
            database.getWordDao().update(word);
//            view.returnToMenuPage();
            view.done();
        }).start();
    }

    public void loadGroups() {
        new Thread(() -> {
            List<Group> groups = database.getGroupDao().getAllGroups();
            view.populateGroups(groups);
        }).start();
    }
}
