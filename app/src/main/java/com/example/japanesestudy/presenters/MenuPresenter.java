package com.example.japanesestudy.presenters;

import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

import androidx.constraintlayout.motion.widget.Debug;

import com.example.japanesestudy.database.AppDatabase;
import com.example.japanesestudy.models.Group;
import com.example.japanesestudy.models.Word;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuPresenter {
    private MVPView view;
    private ArrayList<String> groups = new ArrayList<>();
    private AppDatabase database;

    public interface MVPView extends BaseMVPView {
        void goToReadingMultipleChoicePage(String group, List<Word> words);
        void goToDefinitionMultipleChoicePage(String group, List<Word> words);
        void goToReadingRecognitionPage(String group);
        void goToDefinitionRecognitionPage(String group);
        void goToFuriganaToKanjiPage(String group);
        void goToENtoJPMultipleChoicePage(String group);
        void goToCreateWordPage();
        void goToEditWordPage();
        void goToEditGroupsPage();
        void populateGroups(List<Group> groups);
        void externalSave(String groups, String words);
    }

    public MenuPresenter(MenuPresenter.MVPView view) {
        this.view = view;

        database = view.getContextDatabase();

    }

    //run this on new thread
    public List<Word> loadWords(String group) {
            return database.getWordDao().getAllWordsInGroup(group);
    }
    //run this on new thread

    public List<Word> loadWordsWithKanji(String group) {
        return database.getWordDao().getAllWordsInGroupWithKanji(group);
    }
    //run this on new thread

    public List<Word> loadAllWordsWithKanji() {
        return database.getWordDao().getAllWordsWithKanji();
        //run this on new thread
    }
    public List<Word> loadAllWords() {
        return database.getWordDao().getAllWords();
    }

    public void goToReadingMultipleChoicePage(String group) {

        new Thread(() -> {
            List<Word> words;
            if (group.toLowerCase().equals("all".toLowerCase())) {
                words = loadAllWordsWithKanji();
            }
            else {
                words = loadWordsWithKanji(group);
            }
            view.goToReadingMultipleChoicePage(group, words);
        }).start();
    }

    public void goToDefinitionMultipleChoicePage(String group) {
        new Thread(() -> {
            List<Word> words;
            if (group.toLowerCase().equals("all".toLowerCase())) {
                words = loadAllWords();
            }
            else {
                words = loadWords(group);
            }
            view.goToDefinitionMultipleChoicePage(group, words);
        }).start();
    }

    public void goToReadingRecognitionPage(String group) {
        view.goToReadingRecognitionPage(group);
    }

    public void goToDefinitionRecognitionPage(String group) {
        view.goToDefinitionRecognitionPage(group);
    }

    public void goToFuriganaToKanjiPage(String group) {
        view.goToFuriganaToKanjiPage(group);
    }

    public void goToENtoJPMultipleChoicePage(String group) {
        view.goToENtoJPMultipleChoicePage(group);
    }

    public void goToStudyPage(String group, String method) {
        //Toast.makeText(this, "Study button pressed", Toast.LENGTH_SHORT).show();
        if (method.equals("Reading Multiple Choice")) {
            goToReadingMultipleChoicePage(group);
        }
        else if (method.equals("Definition Multiple Choice")) {
            goToDefinitionMultipleChoicePage(group);
        }
        else if (method.equals("Reading Recognition")) {
            goToReadingRecognitionPage(group);
        }
        else if (method.equals("Definition Recognition")) {
            goToDefinitionRecognitionPage(group);
        }
        else if (method.equals("Furigana To Kanji")) {
            goToFuriganaToKanjiPage(group);
        }
        else if (method.equals("EN to JP Multiple Choice")) {
            goToENtoJPMultipleChoicePage(group);
        }
        else {
            Log.e("missing info", "Unsure of what method to use");
        }

    }

    public void goToCreateWordPage() {
        view.goToCreateWordPage();
    }

    public void goToEditWordPage() {
        view.goToEditWordPage();
    }

    public void goToEditGroupsPage() {
        view.goToEditGroupsPage();
    }

    public void onEditGroupsCustomContractResult() {
        loadGroups();
    }

    public void loadGroups() {
        new Thread(() -> {
            List<Group> groups = database.getGroupDao().getAllGroups();
            view.populateGroups(groups);
        }).start();
    }

    public void loadWordsFromFile(Context context, int resourceId) {
        new Thread(() -> {
            database.getWordDao().nukeData();
            InputStream inputStream = context.getResources().openRawResource(resourceId);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            try {
                String line = reader.readLine();
                while (line != null) {
                    if (line.startsWith("#") || line.length() == 0) {
                        //ignore this line
                        line = reader.readLine();
                        continue;
                    }
                    String[] data = line.split("@");
                    if (data.length != 8) {
                        Log.d("Data Error", "Incorrect data entry: " + line);
                        line = reader.readLine();
                        continue;
                    }
                    Word word = new Word();
                    word.characters = data[0];
                    word.romanization = data[1];
                    word.furigana = data[2];
                    word.okurigana = data[3];
                    word.definition = data[4];
                    word.exampleSentence = data[5];
                    word.group = data[6];
                    word.hasKanji = data[7].toLowerCase().equals("true");
                    database.getWordDao().insert(word);
                    line = reader.readLine();
                }
            }
            catch (Exception e) {
                Log.d("error", "error loading data");
            }

        }).start();
    }

    public void loadGroupsFromFile(Context context, int resourceId) {
        new Thread(() -> {
            database.getGroupDao().nukeData();
            InputStream inputStream = context.getResources().openRawResource(resourceId);
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            try {
                String line = reader.readLine();
                while (line != null) {
                    Group group = new Group();
                    group.name = line;
                    database.getGroupDao().insert(group);
                    line = reader.readLine();
                }
            }
            catch (Exception e) {
                Log.d("error", "error loading data");
            }

        }).start();
    }

    public void externalSave() {
        new Thread(() -> {
            List<Group> groups = database.getGroupDao().getAllGroups();
            List<Word> words = database.getWordDao().getAllWords();

            //TODO format data into an @ delimited string

            String groupsString = prepareForFileGroups(groups);
            String wordsString = prepareForFileWords(words);

            view.externalSave(groupsString, wordsString);
        }).start();
    }

    public String prepareForFileGroups(List<Group> groups) {
        StringBuilder groupsString = new StringBuilder();
        for (Group group : groups) {
            groupsString.append(group.name);
            groupsString.append("\n");
        }

        return groupsString.toString();
    }

    public String prepareForFileWords(List<Word> words) {
        StringBuilder wordsString = new StringBuilder();
        for (Word word : words) {
            wordsString.append(word.characters);
            wordsString.append("@");
            wordsString.append(word.romanization);
            wordsString.append("@");
            wordsString.append(word.furigana);
            wordsString.append("@");
            wordsString.append(word.okurigana);
            wordsString.append("@");
            wordsString.append(word.definition);
            wordsString.append("@");
            wordsString.append(word.exampleSentence);
            wordsString.append("@");
            wordsString.append(word.group);
            wordsString.append("@");
            wordsString.append(word.hasKanji.toString());
            wordsString.append("\n");
        }

        return wordsString.toString();
    }


}
